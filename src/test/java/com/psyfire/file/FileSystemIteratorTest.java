package com.psyfire.file;

import org.junit.Test;

import java.io.File;
import java.util.stream.Stream;

public class FileSystemIteratorTest {
	//TODO change this to whatever location you wish to scan
	private static final String path =".\\src\\test\\resources";

	@Test
	public void testIterator() {
		//TODO: Not a real unit test.
		Stream<File> fileStream = new FileSystemIterator(new File(path)).toStream();
		fileStream.forEach(file -> System.out.println(file.getAbsoluteFile()));
	}
}
