package com.psyfire.duplicateFinder;

import org.junit.Ignore;
import org.junit.Test;

import java.io.*;
import java.util.*;
import java.util.stream.IntStream;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

public class DuplicateFileFinderTest {
	@Test @Ignore //Not a real unit test.
	public void print_duplicates_json() {
		String path =".\\src\\test\\resources";

		File root = new File(path);

		Map<Integer, Set<File>> duplicates = DuplicateFileFinder.findDuplicates(root);

		String json = DuplicatesJsonSerializer.duplicatesJson(duplicates);
		System.out.println(json);
	}

	@Test
	public void duplicate_finder_test() {
		String path =".\\src\\test\\resources\\duplicate_test";

		//Safety check.  All files at this locations are deleted at the end of hte test.
		// If you wish to use this path & get this error, delete the folder manually first.
		assertFalse("Path \"" + path + "\" already exists, aborting.",
				new File(path).exists());

		try {
			List<Integer> dupsByIndex = Arrays.asList(1, 5, 10, 6, 4, 15, 1, 1, 12, 13, 134, 6, 8, 15, 23, 45);

			int totalMatches = 0;
			int numUniqueDuplicates = 0;

			for (int i = 0; i < dupsByIndex.size(); i++) {
				Integer numFiles = dupsByIndex.get(i);
				generateDuplicateFiles(dupsByIndex.get(i), path, "_"+String.format("%03d",i));

				//collect totals
				if (numFiles > 1) {
					totalMatches += numFiles;
					numUniqueDuplicates++;
				}
			}

			Map<Integer, Set<File>> duplicates = DuplicateFileFinder.findDuplicates(new File(path));

			//validate actual totals matche expected
			assertThat(duplicates.size(), is(numUniqueDuplicates));
			assertThat((int) duplicates.values().stream().flatMap(Collection::stream).count(),
					is(totalMatches));

			duplicates.values().forEach(files -> {
				int index = determineIndex(files.stream().findFirst().orElseGet(null));

				//Validate correct number of dups for this index
				assertThat(files.size(), is(dupsByIndex.get(index)));

				//validate all dups are of correct index
				files.forEach(file -> {
					assertThat(determineIndex(file), is(index));
				});
			});

			String json = DuplicatesJsonSerializer.duplicatesJson(duplicates);
			System.out.println(json);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			//cleanup temp files
			deleteFileTree(new File(path));
		}
	}

	/* Last 3 chars of path are it's index.*/
	private int determineIndex(File file) {
		String path = file.getPath();
		return Integer.parseInt(path.substring(path.length() - 3));
	}

	private Collection<String> generateDuplicateFiles(int numDups, String prefix, String suffix) {
		Collection<String> names = randPaths(numDups, prefix, suffix);
		byte[] data = randomBytes(randomInt(200000, 200005));
		createFiles(data, names);
		return names;
	}

	private Collection<String> randPaths(int numNames, String prefix, String suffix) {
		List<String> list = new ArrayList<>();
		IntStream.range(0, numNames).forEach(i-> {
			list.add(prefix + "\\" + randLetterString(1) +"\\" + randLetterString(3) + suffix);
		});
		return list;
	}

	private void createFiles(byte[] bytes, Collection<String> paths) {
		paths.forEach(path -> {
			new File(path).getParentFile().mkdirs();
			try (FileOutputStream fos = new FileOutputStream(path)){
				fos.write(bytes);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	private void deleteFileTree(File file) {
		File[] contents = file.listFiles();
		if (contents != null) {
			for (File f : contents) {
				deleteFileTree(f);
			}
		}
		file.delete();
	}

	/* ------------------------- Random Shortcuts --------------------- */

	private String randLetterString(int length) {
		char[] data = new char[length];
		for (int i = 0; i < length; i++) {
			 data[i] = randLetter();
		}
		return String.copyValueOf(data);
	}

	private char randLetter() {
		Random r = new Random();
		return (char)(r.nextInt(26) + 'a');
	}

	private byte[] randomBytes(int length) {
		byte[] b = new byte[length];
		new Random().nextBytes(b);
		return b;
	}

	private int randomInt(int min, int max) {
		return new Random().nextInt(max - min + 1) + min;
	}


}
