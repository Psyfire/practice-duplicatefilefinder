package com.psyfire.functional;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class CollectorUtils {
	/* Shortcut for collecting a map from a Stream<Map.Entry> */
	public static <K,V> Collector<Map.Entry<K,V>, ?, Map<K,V>> entryToMap() {
		return Collectors.toMap(
				Map.Entry::getKey,
				Map.Entry::getValue
		);
	}

	/* Shortcut for collecting a Map<Key, Set<Value>> from a Stream. */
	public static <T, K, V>  Collector<T, ?, Map<K,Set<V>>>  mapSetCollector(
			Function<? super T, ? extends K> keyMapper,
			Function<? super T, ? extends V> valueMapper) {
		return Collectors.toMap(
				keyMapper,
				(input) -> {
					HashSet<V> set = new HashSet<>();
					set.add(valueMapper.apply(input));
					return set;
				},
				(old, latest)->{
					old.addAll(latest);
					return old;
				}
		);
	}
}
