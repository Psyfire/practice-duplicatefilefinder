package com.psyfire.duplicateFinder;

import com.psyfire.functional.CollectorUtils;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

/* Duplicate finder is used to find likely matches using imperfect but fast functions for determining a likely match.
* For example, one may wish to find matching files, by hashing only part of a file's contents.*/
public class DuplicateFinder {
	/* Matcher-Functions calculate a hash, determining two objs are likely identical.
	 * Matcher-Functions should start very fast & then progressively increase in accuracy. */
	public static <T> Map<Integer, Set<T>> findDuplicates(Stream<T> stream, Collection<Function<T, Integer>> matchers) {
		Map<Integer, Set<T>> results = null;

		for (Function<T, Integer> fn : matchers) {
			results = mapTs(stream, fn);
			stream = streamMatches(results);
		}

		return filterNoMatches(results);
	}

	/* Converts a set of results into a stream, filtering out any results which have no matches.*/
	private static <T> Stream<T> streamMatches(Map<Integer, Set<T>> results) {
		return results.entrySet().stream()
				.filter(hasMatches())
				.flatMap(entry -> entry.getValue().stream());
	}

	/* From a Stream of Ts, map that stream based on given function.  For example, map all files to their file-size, or their hash.*/
	private static <V, K> Map<V, Set<K>> mapTs(Stream<K> stream, Function<K, V> keyFunction) {
		return stream
				.collect(CollectorUtils.mapSetCollector(
						keyFunction,
						file -> file));
	}

	private static <K,V> Predicate<Map.Entry<K, Set<V>>> hasMatches() {
		return entry -> entry.getValue().size() > 1;
	}

	private static <K, V> Map<K, Set<V>> filterNoMatches(Map<K, Set<V>>  map) {
		return map.entrySet().stream()
				.filter(hasMatches())
				.collect(CollectorUtils.entryToMap());
	}
}
