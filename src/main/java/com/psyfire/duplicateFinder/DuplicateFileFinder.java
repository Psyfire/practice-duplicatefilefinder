package com.psyfire.duplicateFinder;

import com.psyfire.file.FileHasher;
import com.psyfire.file.FileSystemIterator;
import java.io.File;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Stream;

public class DuplicateFileFinder {
	public static final String ERR_NOT_FOUND = "File not found.";
	public static final String ERR_NOT_DIR = "Root file must be a directory.";

	private static List<Function<File, Integer>> defaultMatchers = Arrays.asList(
		file -> (int) file.length(),
		file -> FileHasher.hashFile(file, 2, 16),
		file -> FileHasher.hashFile(file, 50, 32),
		file -> FileHasher.hashFile(file, 50, 32)
	);

	public static Map<Integer, Set<File>> findDuplicates(File root) {
		validateRootDir(root);

		Stream<File> fileStream = new FileSystemIterator(root).toStream()
				.filter(File::isFile);

		return findDuplicates(fileStream);
	}


	public static Map<Integer, Set<File>> findDuplicates(Stream<File> fileStream) {
		return findDuplicates(fileStream, defaultMatchers);
	}

	/* Matcher-Functions calculate a hash, determining two files are likely identical.
	 * Matcher-Functions can be anything, such as file-size, or a hash of file contents.
	 * Matcher-Functions should start very fast & then progressively increase in accuracy. */
	public static Map<Integer, Set<File>> findDuplicates(Stream<File> fileStream, Collection<Function<File, Integer>> matchers) {
		return DuplicateFinder.findDuplicates(fileStream, matchers);
	}

	private static void validateRootDir(File root) {
		if (root == null || !root.exists()) {
			throw new IllegalArgumentException(ERR_NOT_FOUND);
		}
		if (!root.isDirectory()) {
			throw new IllegalArgumentException(ERR_NOT_DIR);
		}
	}

}
