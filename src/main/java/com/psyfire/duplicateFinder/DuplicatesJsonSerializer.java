package com.psyfire.duplicateFinder;

import java.io.File;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class DuplicatesJsonSerializer {

	public static <T> String duplicatesJson(Map<T, Set<File>> duplicatesMap) {
		//TODO: Note: Better to use existing JSON Utility like GSON or Jackson.  Currently avoided to reduce dependencies.

		StringBuilder sb = new StringBuilder();
		sb.append("{")
				.append("\"duplicates\":")
				.append("[");
		duplicatesMap.values().forEach(value ->
				sb
						.append(filesToJson(value))
						.append(","));
		if (duplicatesMap.size() > 0) {
			sb.deleteCharAt(sb.length() -1); /* remove last comma */
		}
		sb.append("]}");
		return sb.toString();
	}

	private static String filesToJson(Collection<File> files) {
		//TODO: Note: Better to use existing JSON Utility like GSON or Jackson.  Currently avoided to reduce dependencies.
		StringBuilder sb = new StringBuilder();
		sb.append("{")
				.append("\"duplicate\":")
				.append("[");
		files.forEach(file -> sb
				.append("{\"filePath\":\"")
				.append(file.getAbsolutePath().replace("\\","/"))
				.append("\"},"));
		if (files.size() > 0) {
			sb.deleteCharAt(sb.length() -1); /* remove last comma */
		}
		sb.append("]}");

		return sb.toString();
	}
}
