package com.psyfire.file;

import java.io.File;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class FileSystemIterator implements Iterator<File> {
	File root;
	File[] branches;
	FileSystemIterator subIterator;
	int index = -1;
	public FileSystemIterator(File rootFile) {
		root = rootFile;
		if (!root.exists()) {
			throw new IllegalArgumentException("File not found: \""+rootFile.getPath()+"\"");
		} else if (root.isDirectory()) {
			branches = root.listFiles();
		} else {
			branches = new File[0];
		}
	}

	@Override
	public boolean hasNext() {
		if (branches == null && index != -1) {
			return false;// edge case on Windows with the recycle bin.
		}
		if (subIterator != null && subIterator.hasNext()) {
			return true;
		}
		if (branches != null && index >= branches.length){
			return false;
		}
		return true;
	}

	/* Navigates in order you might expect a fully expanded file system "tree". Folder is returned first, then the contents of the folder.*/
	@Override
	public File next() {
		if (!hasNext()) throw new NoSuchElementException(root.getAbsolutePath());

		if (subIterator != null && subIterator.hasNext()) {
			return subIterator.next();
		}

		/* -1 index to be root, since we want to display that first. */
		if (index == -1) {
			index++;
			return root;
		}

		try{
			subIterator = new FileSystemIterator(branches[index]);
		} catch (Exception e) {
			System.out.println(root.getAbsolutePath());
			e.printStackTrace();
		}
		index++;
		return subIterator.next();
	}

	public Stream<File> toStream() {
		Iterable<File> iterable = () -> this;
		return StreamSupport.stream(iterable.spliterator(), false);
	}
}
