package com.psyfire.file;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;

public class FileHasher {

	public static final String ERR_GENERIC = "Unable to hash file.";

	/**
	 Hashes parts of a file, consistently/evenly spaced throughout that file.
	Performance: Total number of byes sampled per file will be (numSamples * bytesPerSample) or file-size, whichever is smaller.
	@param  numSamples: The number of samples taken spaced evenly throughout the file.
	@param bytesPerSample: The number of bytes sampled at each sample.
	@return Hash of bytes dispersed throughout the file.
	*/
	public static final Integer hashFile(File file, int numSamples, int bytesPerSample) {
		try {
			if (file == null) {return null;}
			if (file.isDirectory()) {return null;}

			long size = file.length();
			if (size == 0) {return 0;}

			RandomAccessFile raf = new RandomAccessFile(file, "r");

			/* Handle small file size edge case. */
			if (size < bytesPerSample * numSamples) {
				return hashEntireFile(raf);
			}

			byte[] byes = new byte[numSamples * bytesPerSample];

			/* Average spacing of samples through file. */
			//Long skipL = (size - bytesPerSample) / numSamples;
			Long skipL = (size - numSamples * bytesPerSample) / numSamples;

			/* Edge case for very large files. */
			if (skipL >= Integer.MAX_VALUE) {
				skipL = (long) (Integer.MAX_VALUE);
			}

			int skip = skipL.intValue();
			for (int i = 0; i < numSamples; i++) {
				raf.read(byes, i*bytesPerSample, bytesPerSample);
				raf.seek(skip);
			}

			raf.close();
			return Arrays.hashCode(byes);
		} catch (Exception e) {
			throw new FileHasherException(ERR_GENERIC,e);
		}
	}

	private static Integer hashEntireFile(RandomAccessFile raf) throws IOException {
		int size = (int) raf.length();
		byte[] byes;
		byes = new byte[size];
		raf.read(byes, 0, size -1);
		return Arrays.hashCode(byes);
	}

	public static class FileHasherException extends RuntimeException {
		public FileHasherException(String message, Exception e) {
			super(message, e);
		}
	}
}


